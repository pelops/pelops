import unittest
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from pelops.abstractmicroservice import AbstractMicroservice


class TestService(AbstractMicroservice):
    _version = 1

    def __init__(self, config, pubsub_client=None, logger=None):
        AbstractMicroservice.__init__(self, config, "test", pubsub_client, logger, logger_name="service",
                                      manage_monitoring_agent=True, no_gui=False)
        self._add_ui_command("hello", self._cmd_hello_world)

    def _start(self):
        pass

    def _stop(self):
        pass

    def _cmd_hello_world(self, args):
        """hello world"""
        print("world")

    @classmethod
    def _get_description(cls):
        return "TestService"

    @classmethod
    def _get_schema(cls):
        schema = {
            "test": {
                "description": "test configuration"
            }
        }
        return schema

    def runtime_information(self):
        return {"test": "test"}

    def config_information(self):
        return {"info": "info"}


def standalone():
    TestService.standalone()


if __name__ == "__main__":
    TestService.standalone()
